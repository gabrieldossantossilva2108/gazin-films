
function irBuscar() {
    window.location.hash = 'search'
}

$(document).ready(() => {
    $('#formBusca').on('submit', (e) => {
        let txtBusca = $('#txtBusca').val();
        getFilmes(txtBusca)
        e.preventDefault();
    });

    if (window.location.reload) {
        window.location.hash = 'home'
    }

    if (window.location.hash == '#home') {
        getImagesBanner();

    } else if (window.location.hash == '#atorDetal') {
        mostrarDetailsAtor();
    }
    else if (window.location.hash == '#filmeDetal') {
        mostrarDetailsFilme();
    }

});



function getFilmes(txt) {
    document.querySelector('.conteudo_pesquisa').innerHTML = '';
    document.querySelector('.loading').innerHTML = 'Carregando...';

    fetch(`https://imdb8.p.rapidapi.com/auto-complete?q=${txt}`, {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "imdb8.p.rapidapi.com",
            "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
        }
    })
        .then(response => response.json())
        .then(data => {
            const list = data.d;

            list.map(item => {
                const nome = item.l;
                const poster = item.i.imageUrl;
                const ano = item.y;
                const id = item.id;
                const filme = `<div class='mb-4 p-2 col-6 col-md-2'>
                <a onclick="movieSelected('${id}')" class='card-film'>
                <img class='img-card' src="${poster}">
                <div class=' text-card'>
                    <p class='text-center'><strong>${nome}</strong></br>
                     ${ano}</p>
                </div>
                </a>
            </div>`;
                document.querySelector('.loading').innerHTML = '';
                document.querySelector('.conteudo_pesquisa').innerHTML += filme;
            })
        })
        .catch(err => {
            console.error(err);
        });
}



function movieSelected(id) {
    if (id.includes('tt')) {
        sessionStorage.setItem('filmeId', id);
        window.location.hash = 'filmeDetal'
        mostrarDetailsFilme()
    }
    else {
        sessionStorage.setItem('atorId', id);
        window.location.hash = 'atorDetal'
        mostrarDetailsAtor()
    }
}

function mostrarDetailsFilme() {

    let movieId = sessionStorage.getItem('filmeId');
    fetch(`https://imdb8.p.rapidapi.com/title/get-overview-details?tconst=${movieId}&currentCountry=PT`, {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "imdb8.p.rapidapi.com",
            "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
        }
    })
        .then(response => response.json())
        .then(data => {

            const list = data.title;
            const rate = data.ratings
            const visao = data.plotOutline

            if (rate.canRate == undefined || rate.canRate == '' || rate.canRate == false || rate.canRate == null) {
                rate.rating = '0.0'
            }

            const nome = list.title;
            const poster = list.image.url;
            const ano = list.year;
            const nota = rate.rating;
            const visao_geral = visao.text;
            // const id = list.id;

            const det = `<div class='ml-3'><img class='img-detail' src='${poster}'></div>
                                <div class='ml-4'>
                                    <p class='mt-5 fdesc'>${nome} <span style='color: #ccc;'>${ano}</span></p>
                                    <p class='fdesc-rt'><img class='star-icon mb-1 mr-1' src='./assets/images/star.png'>${nota}/10  <span class='imdb'>IMDb</span></p>
                                </div>`;

            const visaoG = ` <h2 class='flogo col-6'>Visão Geral</h2> 
                    <p class='col-10 col-md-6 fdesc-rt'>${visao_geral}</p>`;

            document.querySelector('.banner_filme_detail').style.background = `url('${poster}')`;
            document.querySelector('.detalhes').innerHTML = det;
            document.querySelector('.visao_geral').innerHTML = visaoG;

        })
        .catch(err => {
            console.error(err);
        });
}


function idade(ano_aniversario, mes_aniversario, dia_aniversario) {
    var d = new Date,
        ano_atual = d.getFullYear(),
        mes_atual = d.getMonth() + 1,
        dia_atual = d.getDate(),

        ano_aniversario = +ano_aniversario,
        mes_aniversario = +mes_aniversario,
        dia_aniversario = +dia_aniversario,

        quantos_anos = ano_atual - ano_aniversario;

    if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
        quantos_anos--;
    }

    return quantos_anos < 0 ? 0 : quantos_anos;
}




function mostrarDetailsAtor() {
    let id = sessionStorage.getItem('atorId');

    fetch(`https://imdb8.p.rapidapi.com/actors/get-bio?nconst=${id}`, {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "imdb8.p.rapidapi.com",
            "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
        }
    })

        .then(response => response.json())
        .then(data => {
            const imagem = data.image.url;
            const nome = data.name;
            const bio = data.miniBios[0].text;



            var data_nasc = new Date(data.birthDate);
            var dia_nasc = data_nasc.getDate()
            var mes_nasc = data_nasc.getMonth()
            var ano_nasc = data_nasc.getFullYear()

            i = idade(ano_nasc, mes_nasc, dia_nasc)

            const local = data.birthPlace;

            const infos = `<div class='img-ator'><img src='${imagem}'></div>
                        <div class='txt-ator ml-4 pt-4'>
                            <p class='nome-ator'>${nome}</p>
                            <p class='font-ator'>${i} anos</p>
                            <p class='font-ator'>${local}</p>
                        </div>`;

            const biografia = `<p class='flogo'>Biografia</p>
                                <p class='font-ator'>${bio}</p>`;

            document.querySelector('.infos-ator').innerHTML = infos;
            document.querySelector('.bio-ator').innerHTML = biografia;
            document.querySelector('.filmo-ator').innerHTML = '';



        })

    fetch(`https://imdb8.p.rapidapi.com/actors/get-all-filmography?nconst=${id}`, {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "imdb8.p.rapidapi.com",
            "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
        }
    })

        .then(response => response.json())
        .then(data => {

            var films = data.filmography;
            films = Array.from(films)

            films.slice(10, 16).map(item => {

                const titulo = item.title;
                const id = item.id.replace('/title/', '').replace('/', '');

                const filmografia = `


                <div class='card-film mb-4 p-2 col-6 col-lg-2'>
                    <a onclick="movieSelected('${id}')" class='mb-2'>
                    <img src="${item.image.url}">
                    <div class=' text-card'>
                        <p class='text-center card-title'><strong>${titulo}</strong>
                    </div>
                    </a>
                </div>


                `;
                document.querySelector('.filmo-ator').innerHTML += filmografia;

            })


        })

        .catch(err => {
            console.error(err);
        });

}






function getFilmesPopulares() {
    fetch("https://imdb8.p.rapidapi.com/title/get-most-popular-movies?homeCountry=US&purchaseCountry=US&currentCountry=US", {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "imdb8.p.rapidapi.com",
            "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
        }
    })
        .then(response => response.json())
        .then(data => {

            id1 = data[0].replace('/title/', '').replace('/', '')
            id2 = data[1].replace('/title/', '').replace('/', '')
            id3 = data[2].replace('/title/', '').replace('/', '')
            id4 = data[3].replace('/title/', '').replace('/', '')
            id5 = data[4].replace('/title/', '').replace('/', '')


            sessionStorage.setItem('idFilme1', id1)
            sessionStorage.setItem('idFilme2', id2)
            sessionStorage.setItem('idFilme3', id3)
            sessionStorage.setItem('idFilme4', id4)
            sessionStorage.setItem('idFilme5', id5)



        })
        .catch(err => {
            console.error(err);
        })

}

function getProxFilme() {
    fetch("https://imdb8.p.rapidapi.com/title/get-coming-soon-movies?homeCountry=US&purchaseCountry=US&currentCountry=US", {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "imdb8.p.rapidapi.com",
            "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
        }
    })
        .then(response => response.json())
        .then(data => {

            id1 = data[0].id.replace('/title/', '').replace('/', '')
            id2 = data[1].id.replace('/title/', '').replace('/', '')
            id3 = data[2].id.replace('/title/', '').replace('/', '')
            id4 = data[3].id.replace('/title/', '').replace('/', '')
            id5 = data[4].id.replace('/title/', '').replace('/', '')


            sessionStorage.setItem('idProxFilme1', id1)
            sessionStorage.setItem('idProxFilme2', id2)
            sessionStorage.setItem('idProxFilme3', id3)
            sessionStorage.setItem('idProxFilme4', id4)
            sessionStorage.setItem('idProxFilme5', id5)


        })
        .catch(err => {
            console.error(err);
        })
}


function getAtorPop() {
    fetch("https://imdb8.p.rapidapi.com/actors/list-most-popular-celebs?homeCountry=US&currentCountry=US&purchaseCountry=US", {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "imdb8.p.rapidapi.com",
            "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
        }
    })
        .then(response => response.json())
        .then(data => {

            id1 = data[1].replace('/name/', '').replace('/', '')
            id2 = data[2].replace('/name/', '').replace('/', '')
            id3 = data[3].replace('/name/', '').replace('/', '')
            id4 = data[4].replace('/name/', '').replace('/', '')
            id5 = data[5].replace('/name/', '').replace('/', '')


            sessionStorage.setItem('idAtor1', id1)
            sessionStorage.setItem('idAtor2', id2)
            sessionStorage.setItem('idAtor3', id3)
            sessionStorage.setItem('idAtor4', id4)
            sessionStorage.setItem('idAtor5', id5)



        })
        .catch(err => {
            console.error(err);
        })
}


function getImagesBanner() {
    getFilmesPopulares()
    getProxFilme()
    getAtorPop()
    for (i = 1; i <= 3; i++) {

        fetch(`https://imdb8.p.rapidapi.com/title/get-all-images?tconst=${sessionStorage.getItem(`idFilme${i}`)}&limit=1`, {
            "method": "GET",
            "headers": {
                "x-rapidapi-host": "imdb8.p.rapidapi.com",
                "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
            }
        })
            .then(response => response.json())
            .then(data => {

                const img = data.resource.images[0].url
                const title = data.resource.images[0].caption
                let id = data.resource.images[0].id;
                id = id.replace('/title/', '').split('/')[0]

                const itemsBanner = `<a onclick="movieSelected('${id}')" class="carousel-item p-0">
          <img src="${img}" class="bannerImgStyl">
          <div class='bannerTxt'>
            <h2>${title}</h2>
          </div>
        </a>`;

                document.querySelector('.bannerimgs').innerHTML += itemsBanner;



            })
            .catch(err => {
                console.error(err);
            });



    }

    setTimeout(function () {
        getFilmesPopularesCards()
    }, 3000)

    setTimeout(function () {
        getProxFilmCards()
    }, 5000)

    setTimeout(function () {
        getPopAtorCards()
    }, 7500)

}

function getFilmesPopularesCards() {

    for (i = 1; i <= 5; i++) {

        fetch(`https://imdb8.p.rapidapi.com/title/get-overview-details?tconst=${sessionStorage.getItem(`idFilme${i}`)}&currentCountry=PT`, {
            "method": "GET",
            "headers": {
                "x-rapidapi-host": "imdb8.p.rapidapi.com",
                "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
            }
        })


            .then(response => response.json())
            .then(data => {

                const list = data.title;


                const nome = list.title;
                const img = list.image.url;
                const ano = list.year;

                const id = list.id.replace('/title/', '').replace('/', '')

                const filmsPop = ` <div class='mb-4 p-2 col-6 col-md-2'>
                                    <a onclick="movieSelected('${id}')" class='card-film'>
                                        <img class='img-card' src="${img}">
                                        <div class=' text-card'>
                                            <p class='text-center'><strong>${nome}</strong></br>
                                            ${ano}</p>
                                        </div>
                                     </a>
                                    </div>`;


                document.querySelector('.filmes_populares').innerHTML += filmsPop;

            })

            .catch(err => {
                console.error(err);
            });


    }

}


function getProxFilmCards() {

    for (i = 1; i <= 5; i++) {

        fetch(`https://imdb8.p.rapidapi.com/title/get-overview-details?tconst=${sessionStorage.getItem(`idProxFilme${i}`)}&currentCountry=PT`, {
            "method": "GET",
            "headers": {
                "x-rapidapi-host": "imdb8.p.rapidapi.com",
                "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
            }
        })


            .then(response => response.json())
            .then(data => {

                const list = data.title;


                const nome = list.title;
                const img = list.image.url;
                const ano = list.year;

                const id = list.id.replace('/title/', '').replace('/', '')

                const filmsPop = ` <div class='mb-4 p-2 col-6 col-md-2'>
                                    <a onclick="movieSelected('${id}')" class='card-film'>
                                        <img class='img-card' src="${img}">
                                        <div class=' text-card'>
                                            <p class='text-center'><strong>${nome}</strong></br>
                                            ${ano}</p>
                                        </div>
                                     </a>
                                    </div>`;


                document.querySelector('.proximos_lancamentos').innerHTML += filmsPop;

            })

            .catch(err => {
                console.error(err);
            });

    }

}

function getPopAtorCards() {
    for (i = 1; i <= 5; i++) {

        fetch(`https://imdb8.p.rapidapi.com/actors/get-bio?nconst=${sessionStorage.getItem(`idAtor${i}`)}`, {
            "method": "GET",
            "headers": {
                "x-rapidapi-host": "imdb8.p.rapidapi.com",
                "x-rapidapi-key": "9923d30c2fmshd1ba236c65ceab1p18a10bjsn1d69fc39062b"
            }
        })

            .then(response => response.json())
            .then(data => {
                const imagem = data.image.url;
                const nome = data.name;
                const id = data.id.replace('/name/', '').replace('/', '');

                var data_nasc = new Date(data.birthDate);
                var dia_nasc = data_nasc.getDate()
                var mes_nasc = data_nasc.getMonth()
                var ano_nasc = data_nasc.getFullYear()

                idad = idade(ano_nasc, mes_nasc, dia_nasc)


                const infos = `
                <a onclick="movieSelected('${id}')" class='card-film ml-4'>
                <div class='img-ator'><img src='${imagem}'></div>
                                <div class='txt-ator'>
                                    <p class='nome-ator text-center'>${nome}</p>
                                    <p class='font-ator text-center'>${idad} anos</p>
                                </div></a>`;

                document.querySelector('.atores_populares').innerHTML += infos;




            })
    }
}