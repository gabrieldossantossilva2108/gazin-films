# Como iniciar o ambiente

## Instalação

Clone o repositório remoto: [https://gitlab.com/gabrieldossantossilva2108/gazin-films](https://gitlab.com/gabrieldossantossilva2108/gazin-films]) na sua pasta htdocs, dentro dos arquivos do XAMPP.

```bash
git clone https://gitlab.com/gabrieldossantossilva2108/gazin-films
```

## Iniciando o servidor local
Após clonar os arquivos, faça os seguintes passos:
- Abra o XAMPP, e inicie o Apache;
- No navegador, insira esse link na url: http://localhost/gazintech_project/src/?#home, ou clique [aqui](http://localhost/gazintech_project/src/?#home).


## Feito por: 
[Gabriel dos Santos Silva](https://www.linkedin.com/in/gabriel-dos-santos-silva-005a19213/) para [GazinTech](https://atendimento.gazin.com.br/kb).
