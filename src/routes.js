import home from "./pages/home/home.js";
import search from "./pages/search/search.js";
import filmeDetal from "./pages/filmeDetal/filmeDetal.js";
import atorDetal from "./pages/atorDetal/atorDetal.js";

export default {
  home: home(),
  search: search(),
  filmeDetal: filmeDetal(),
  atorDetal: atorDetal(),
}