export default () => {
  const container = document.createElement('div');


  const template = `
    
    <div class="container-fluid">
      <!-- slider -->
      <div id="mainSlider" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner bannerimgs">
          <div class='nota'></div>
            <div class="carousel-item  active">
                <img class='bannerImgStyl' src='./assets/images/filmes_momento.png'>
            </div>
          </div>
          <a class="carousel-control-prev" href="#mainSlider" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#mainSlider" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
      </div>
    </div>


    <div class='container linha_filmsPop'>
      <p class='flogo text-center'>Filmes Populares</p>
      <div class='filmes_populares row justify-content-center'>
      
      </div>

    </div>


    <div class='container linha_filmsPop mt-4'>
      <p class='flogo text-center'>Próximos Lançamentos</p>
      <div class='proximos_lancamentos row justify-content-center'>

      </div>
    </div>

    <div class='container linha_filmsPop mt-4'>
    <p class='flogo text-center'>Atores Populares</p>
    <div class='atores_populares row justify-content-center'>
  
    </div>

  </div>
`;


  container.innerHTML = template;

  return container;
}